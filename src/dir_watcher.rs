
pub mod watcher {
    use notify::{Watcher, RecursiveMode, watcher};
    use std::sync::mpsc::channel;
    use std::time::Duration;

    pub fn watch(path: String, duration: u64) {
            let (tx, rx) = channel();
            let mut watcher = watcher(tx, Duration::from_secs(duration)).unwrap();
            watcher.watch(path, RecursiveMode::Recursive).unwrap();   
            loop {
                match rx.recv() {
                Ok(event) => println!("{:?}", event),
                Err(e) => println!("watch error: {:?}", e),
                }
            }
    }
}